import mongoose from 'mongoose'

// Create Product Schema
const productSchema = mongoose.Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId, // Gets id of User
			required: false,
			ref: 'User', // Adds relationship between Product and User
		},
		name: {type: String, required: true },
		image: {type: String, required: true},
		description: {type: String, required: true},
		price: { type: Number, required: true, default: 0},
		countInStock: {type: Number, required: true, default: 0},
		dateCreated: {type: Date, default: Date.now}
	},
)

const Product = mongoose.model('Product', productSchema);


export default Product
