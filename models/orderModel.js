import mongoose from 'mongoose'

// Create item Schema
const orderSchema = mongoose.Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId, // Gets id of User
			required: false,
			ref: 'User', // Adds relationship between Order and User
		},
		orderItems:  [
			{
				quantity: { type: Number, required: false },
				product: {
					type: mongoose.Schema.Types.ObjectId, // Gets id of Product
					required: false,
					ref: 'Product', // Adds relationship between Order and Product },
				},
			},
		],
		shippingAddress:{type: String, required: false},
		city:{type: String, required:false},
		phone:{type: String, required:false},
		totalPrice:{type: Number, required:false, default: 0.0,},
		orderStatus: {type: String, required: true, default: "Processing"},
		dateOrdered: {type: Date, default: Date.now}
		
	}
)

const Order = mongoose.model('Order', orderSchema)

export default Order
