import mongoose from 'mongoose'


const itemSchema = mongoose.Schema(
	{
		product: {
			type: mongoose.Schema.Types.ObjectId, // Gets id of User
			required: false,
			ref: 'Product', // Adds relationship between product and order
		},
		quantity:{type: String, required: false},
		
	},
)

const Order = mongoose.model('Item', itemSchema)

export default Order