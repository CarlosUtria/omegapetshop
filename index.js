import express from 'express'
import dotenv from 'dotenv'
import connectDB from './config/connection.js'
import cors from 'cors'

// import routes
import productRoutes from './routes/productRoutes.js'
import userRoutes from './routes/userRoutes.js'
import orderRoutes from './routes/orderRouter.js'


//config
const app = express();
app.use(cors());
dotenv.config()
connectDB()

//middleware
app.use(express.json())
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/orders', orderRoutes)




app.listen(process.env.PORT || 4000, () => {
  console.log("server is running!");
});