import mongoose from 'mongoose'

const connectDB = () => {
        mongoose.connect(process.env.MONGO_URL)
        .then(()=>console.log("BD Successfull"))
        .catch(err => console.log("BD Error"));
}

export default connectDB