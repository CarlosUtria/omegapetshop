import jwt from 'jsonwebtoken'

function verifyToken(req, res, next) {

    const requestHeader = req.headers['authorization'];

    if (typeof requestHeader !== 'undefined') {
        const token = requestHeader.split(" ")[1];

        jwt.verify(token, 'JWT_SECRET', (err, payload) => {

            if (err) {
                res.status(403).send({
                    error: "Token not valid"
                });
            } else {
                req.data = payload;
                next();
            }
        });

    } else {
        res.status(403).send({
            error: "Token missing"
        });
    }

}

export default verifyToken