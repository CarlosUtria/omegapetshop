import React from 'react';
import '../styles/components/Footer.css'

const Footer = () => {
  return (
    <div className="Footer">
      <p className="Footer-title">Omega Pet Shop</p>
      <p className="Footer-copy">todos los derechos reservados para el grupo 9</p>
    </div>
  );
}

export default Footer;