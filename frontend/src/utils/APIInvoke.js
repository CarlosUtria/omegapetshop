

class APIInvoke {

    async invokeGET(resource) {

        const data = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = `${resource}`
        let response = (await (await fetch(url, data)).json())
        return response
    }

    async invokePUT(resource, body) {

        const data = {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = `${resource}`
        let response = (await (await fetch(url, data)).json())
        return response
    }

    async invokePOST(resource, body) {

        const data = {
            method: 'POST',
            body: JSON.stringify( body ),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = `${resource}`;
        let response = (await (await fetch(url, data)).json());
        return response;
    }

    async invokeDELETE(resource) {

        const data = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = `${resource}`
        let response = (await (await fetch(url, data)).json())
        return response
    }
}

export default new APIInvoke();