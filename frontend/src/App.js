import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React, { Fragment } from 'react';
import AppContext from './context/AppContext';
import useInitialState from './context/useInitialState';

//components
import Layout from './components/Layout';

//pages
import Home from './pages/Home';
import Checkout from './pages/Checkout';
import Information from './pages/Information';
import Payment from './pages/Payment';
import Login from './pages/Login';
import Register from './pages/Register';
import NewProduct from './pages/Admin/NewProduct';
import ProductList from './pages/Admin/ProductList';
import ProductEdit from './pages/Admin/editProduct';



function App() {
  const initialState = useInitialState();

  return (
    <Fragment>
      <AppContext.Provider value={initialState}>
      <Router>
        <Layout>
          <Routes>
            <Route path='/' exact element={ <Home /> } />
            <Route path='/checkout' exact element={ <Checkout /> } />
            <Route path='/checkout/information' exact element={ <Information /> } />
            <Route path='/checkout/payment' exact element={ <Payment /> } />
            <Route path='/login' exact element={ <Login /> } />
            <Route path='/register' exact element={ <Register /> } />
            <Route path='/newProduct' exact element={ <NewProduct /> } />
            <Route path='/productList' exact element={ <ProductList /> } />
            <Route path='/edit/product/:id' exact element={ <ProductEdit /> } />
          </Routes>
        </Layout>
      </Router>
      </AppContext.Provider>
    </Fragment>
  );
}

export default App;
