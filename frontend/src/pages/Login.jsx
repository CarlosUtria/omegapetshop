
import React, { useState} from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../utils/APIInvoke';
import swal from 'sweetalert'




const Login = () => {

    const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

    const data = {
        email: email,
        password: password
    };

	const submitHandler = async (e) => {
		e.preventDefault()
		// login
        const response = await APIInvoke.invokePOST(`http://localhost:4000/api/users/login`, data);
        
        if (response.message === 'logueado') {
            const msg = "logueado correctamente";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }

        if (response.message === 'usuario no encontrado') {
            const msg = "usuario no encontrado";
            swal({
                title: 'Información',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }
	}


    return (
        <div className="hold-transition login-page">
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to={"#"} className="h1"><b>Log in</b></Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Sign in to start your session</p>
                        
                        <form onSubmit={submitHandler}>

                            <div className="input-group mb-3">
                                
                                <input type="email" 
                                className="form-control" 
                                placeholder="Email" 
                                value={email}
						        onChange={(e) => setEmail(e.target.value)}
                                required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" 
                                className="form-control" 
                                placeholder="Password"
                                value={password}
						        onChange={(e) => setPassword(e.target.value)}
                                required 
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                
                            <div className="social-auth-links text-center mt-2 mb-3">
                                <button type="submit" className="btn btn-block btn-primary">
                                    Log in 
                                </button>
                                <Link to={"/register"} className="btn btn-block btn-success">
                                    Register
                                </Link>
                            </div>

                        </form>
                    
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Login;