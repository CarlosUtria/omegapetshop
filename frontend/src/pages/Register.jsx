import React, { useState} from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../utils/APIInvoke';
import swal from 'sweetalert'


const Register = () => {
	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')

    const data = {
        name: name ,
        email: email,
        password: password,
    };


    
    const submitHandler = async (e) => {
		e.preventDefault()
		// Check if passwords match
		if (password !== confirmPassword) {
			const msg = "passwords y confirpasswords diferentes";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                }
            }
        });
		} else {
			// Dispatch register

			const response = await APIInvoke.invokePOST(`http://localhost:4000/api/users/register`, data);
            
            if (response.message === 'usuario registrado') {
                const msg = "usuario registrado";
                swal({
                    title: 'Información',
                    text: msg,
                    icon: 'success',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-primary',
                            closeModal: true
                        }
                    }
                });
            }

            if (response.message === 'Email ya registrado') {
                const msg = "usuario ya registrado";
                swal({
                    title: 'Información',
                    text: msg,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        }
                    }
                });
            }



		}
	}
    
    return (
        <div className="hold-transition register-page">

            <div className="register-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to={"#"} className="h1"><b>New account</b> </Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Register a new account</p>
                        <form onSubmit={ submitHandler }>
                            <div className="input-group mb-3">
                                <input 
                                type="text" 
                                className="form-control" 
                                placeholder="Full name"
                                value={name}
						        onChange={(e) => setName(e.target.value)}
                                required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input 
                                type="email" 
                                className="form-control" 
                                placeholder="Email"
                                value={email}
						        onChange={(e) => setEmail(e.target.value)}
                                required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input 
                                type="password" 
                                className="form-control" 
                                placeholder="Password" 
                                value={password}
						        onChange={(e) => setPassword(e.target.value)}
                                required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input 
                                type="password" 
                                className="form-control" 
                                placeholder="Retype password"
                                value={confirmPassword}
						        onChange={(e) => setConfirmPassword(e.target.value)}
                                required
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="social-auth-links text-center">
                                <button type="submit" className="btn btn-block btn-primary">
                                    Register
                                </button>
                                <Link to={"/"} className="btn btn-block btn-danger">
                                    Back
                                </Link>
                                </div>

                        </form>

                    </div>
                </div>
            </div>

        </div>
    );

};

export default Register;