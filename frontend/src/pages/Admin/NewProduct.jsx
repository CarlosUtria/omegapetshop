import React, { useState } from "react";
import "../../styles/components/newProduct.css";
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert'

const NewProduct = () => {

  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [Stock, setStock] = useState(0);
  const [image, setImage] = useState("")

  const data = {
    name: name ,
    price: price,
    description: description,
    countInStock: Stock,
    image: image,
    
};

const submitHandler = async (e) => {
  e.preventDefault()
    //register
    const response = await APIInvoke.invokePOST(`http://localhost:4000/api/products/create`, data);

    if (response.message === 'created Product') {
      const msg = "El producto fue creado correctamente";
      swal({
          title: 'Información',
          text: msg,
          icon: 'success',
          buttons: {
              confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-primary',
                  closeModal: true
              }
          }
      });


  } else {
      const msg = "El producto no fue creado correctamente.";
      swal({
          title: 'Error',
          text: msg,
          icon: 'error',
          buttons: {
              confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-danger',
                  closeModal: true
              }
          }
      });
  }

}


return (
  <div className="hold-transition register-page">

      <div className="register-box">
          <div className="card card-outline card-primary">
              <div className="card-body">
                  <p className="login-box-msg">Register a new product</p>
                  <form onSubmit={ submitHandler }>
                      <div className="input-group mb-3">
                          <input 
                          type="text" 
                          className="form-control" 
                          placeholder="Product Name"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                          required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-user" />
                              </div>
                          </div>
                      </div>
                      <div className="input-group mb-3">
                          <input 
                          type="number" 
                          placeholder="price"
                          onChange={(e) => setPrice(e.target.value)}
                          required
                          />
                      </div>
                      <div className="input-group mb-3">

                          <textarea 
                          className="form-control" 
                          placeholder="Product Description" 
                          value={description}
                          onChange={(e) => setDescription(e.target.value)}
                          required
                          cols="30"
                          rows="1"
                          ></textarea>
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>
                      
                      <div className="input-group mb-3">
                          <input 
                          type="number" 
                          className="form-control" 
                          placeholder="Stock"
                          onChange={(e) => setStock(e.target.value)}
                          required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>

                      <div className="input-group mb-3">
                          <input 
                          type="text" 
                          className="form-control" 
                          placeholder="image Name"
                          value={image}
                          onChange={(e) => setImage(e.target.value)}
                          required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>

                      <div className="social-auth-links text-center">
                          <button type="submit" className="btn btn-block btn-primary">
                              create Product
                          </button>
                          <Link to={"/productList"} className="btn btn-block btn-danger">
                              Product Lists
                          </Link>
                          </div>

                  </form>

              </div>
          </div>
      </div>

  </div>
);

};

export default NewProduct;
