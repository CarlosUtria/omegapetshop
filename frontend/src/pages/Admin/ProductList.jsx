import React, { useState, useEffect } from "react";
import { Table, Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import axios from 'axios'
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert'


const ProductList = () => {

	const [products, setProducts] = useState([]);

	const cargarProductos = async()=>{
		const { data } = await axios.get(`http://localhost:4000/api/products`)
		setProducts(data)
	}

	const deleteHandler = async (e, idProducto) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`http://localhost:4000/api/products/delete/${idProducto}`);

        if (response.message === 'Product removed') {
            const msg = "El producto fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarProductos();
        } else {
            const msg = "El producto no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

	useEffect(() => {
        cargarProductos();
    }, [])



	
	return (
		<>
			<Row className='align-items-center'>
				<Col>
					<h1>Products</h1>
				</Col>
				<Col className='text-right'>
					<Link to="/newProduct">
						<Button className='my-3'>
							<span className='plus-sign-margin'>
								<i className='fas fa-plus'></i>
							</span>
							Create Product
					</Button>
					</Link>
				</Col>
			</Row>
				<>
					<Table bordered hover responsive className='table-sm'>
						<thead>
							<tr>
								<th>ID</th>
								<th>title</th>
								<th>Price</th>
								<th>description</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							{products.map((product) => (
								<tr key={product._id}>
									<td>{product._id}</td>
									<td>{product.name}</td>
									<td>{product.price}</td>
									<td>{product.description}</td>
									
									{/* Edit button */}
									<td>
										<Link to={`/edit/product/${product._id}`}>
											<Button variant='light' className='btn-sm'>
												<i className='fas fa-edit'></i>
											</Button>
										</Link>
									</td>
									{/* Delete button */}
									<td>
										<Button
											variant='danger'
											className='btn-sm'
											onClick={(e) => deleteHandler(e, product._id)}
										>
											<i className='fas fa-trash'></i>
										</Button>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
					
				</>
			
		</>
	)
}

export default ProductList
