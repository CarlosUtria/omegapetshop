import React, { useState, useEffect } from "react";
import "../../styles/components/newProduct.css";
import { Link, useParams } from 'react-router-dom';
import axios from 'axios'
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert'


const ProductEdit = () => {

    let {id} = useParams();
  

    const [productId, setProductId] = useState('');

	const cargarProducto = async()=>{
		const { data } = await axios.get(`http://localhost:4000/api/products/${id}`)
		setProductId(data)
	}

    const { name, description, image, price, countInStock} = productId;


    const onChange = ( event ) => {
        setProductId(
            {
                ...productId,
                [ event.target.name ] : event.target.value
            }
        );
    }

    const data = {
        name: productId.name ,
        price: productId.price,
        description: productId.description,
        countInStock: productId.Stock,
        image: productId.image,
        
    };



const submitHandler = async (e) => {
    e.preventDefault()
      //register
      const response = await APIInvoke.invokePUT(`http://localhost:4000/api/products/update/${id}`, data);
  
      if (response.message === 'created Product') {
        const msg = "El producto no fue actualizado";
        swal({
            title: 'Error',
            text: msg,
            icon: 'error',
            buttons: {
                confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                }
            }
        });
  
  
    } else {
        const msg = "El producto fue actualizado.";

        swal({
            title: 'Información',
            text: msg,
            icon: 'success',
            buttons: {
                confirm: {
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-primary',
                    closeModal: true
                }
            }
        });
        
    }
  
  }

  useEffect(() => {
    cargarProducto();
}, [])


return (
  <div className="hold-transition register-page">

      <div className="register-box">
          <div className="card card-outline card-primary">
              <div className="card-body">
                  <p className="login-box-msg">Register a new product</p>
                  <form onSubmit={ submitHandler}>
                      <div className="input-group mb-3">
                          <input 
                            type="text" 
                            className="form-control" 
                            placeholder="Product Name"
                            id="name"
                            name="name"
                            value={ name }
                            onChange={ onChange }
                            required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-user" />
                              </div>
                          </div>
                      </div>
                      <div className="input-group mb-3">
                          <input 
                            type="number" 
                            placeholder="price"
                            id="price"
                            name="price"
                            value={price}
                            onChange={onChange}
                            required
                          />
                      </div>
                      <div className="input-group mb-3">

                          <textarea 
                            className="form-control" 
                            placeholder="Product Description" 
                            id="description"
                            name="description"
                            value={description}
                            onChange={onChange}
                            required
                            cols="30"
                            rows="1"
                          ></textarea>
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>
                      
                      <div className="input-group mb-3">
                          <input 
                            type="number" 
                            className="form-control" 
                            placeholder="Stock"
                            id="countInStock"
                            name="countInStock"
                            value={countInStock}
                            onChange={onChange}
                            required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>

                      <div className="input-group mb-3">
                          <input 
                            type="text" 
                            className="form-control" 
                            placeholder="image Name"
                            id="image"
                            name="image"
                            value={image}
                            onChange={onChange}
                            required
                          />
                          <div className="input-group-append">
                              <div className="input-group-text">
                                  <span className="fas fa-lock" />
                              </div>
                          </div>
                      </div>

                      <div className="social-auth-links text-center">
                          <button type="submit" className="btn btn-block btn-primary">
                              update Product
                          </button>
                          <Link to={"/productList"} className="btn btn-block btn-danger">
                              Product Lists
                          </Link>
                      </div>

                  </form>

              </div>
          </div>
      </div>

  </div>
);

};

export default ProductEdit;