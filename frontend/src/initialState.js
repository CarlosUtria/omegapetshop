export default {
  cart: [],
  buyer: [],
  products: [
    {
      'id': '1',
      'image': 'https://cdn.pixabay.com/photo/2016/10/31/14/55/rottweiler-1785760__340.jpg',
      'title': '2x1',
      'price': 25,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
    {
      'id': '3',
      'image': 'https://cdn.pixabay.com/photo/2017/04/11/15/55/animals-2222007__340.jpg',
      'title': 'gato',
      'price': 10,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
    {
      'id': '4',
      'image': 'https://cdn.pixabay.com/photo/2017/09/25/13/12/puppy-2785074__340.jpg',
      'title': 'perrito',
      'price': 4,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
    {
      'id': '5',
      'image': 'https://cdn.pixabay.com/photo/2018/02/19/10/22/treats-3164687__340.jpg',
      'title': 'figurritas',
      'price': 2,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
    {
      'id': '6',
      'image': 'https://cdn.pixabay.com/photo/2014/03/07/10/03/food-bowl-281980__340.jpg',
      'title': 'comida generica',
      'price': 2,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
    {
      'id': '7',
      'image': 'https://cdn.pixabay.com/photo/2021/02/15/16/01/pet-food-6018384__340.jpg',
      'title': 'comida vip',
      'price': 35,
      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    },
  ],
};