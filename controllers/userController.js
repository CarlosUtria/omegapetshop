import User from '../models/userModel.js'
import bcrypt from 'bcrypt'
import generateToken from '../service/generateToken.js'


//Register 
const registerUser = async (req, res) => {
	const { email } = req.body
	const isEmailExist = await User.findOne({ email: email })

    if (isEmailExist) {
        return res.status(400).json({message: 'Email ya registrado'})
    }

    // hash
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: password
    });
    try {
        const savedUser = await user.save();
        res.json({message: 'usuario registrado'})
    } catch (error) {
        res.status(400).json({message: error})
    }
}




  //Login
  const authUser = async (req, res) => {
	const { email, password } = req.body
	const user = await User.findOne({ email: email })

	if (user && (await bcrypt.compare(password, user.password))) {
		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
			token: generateToken(user._id),
            message: 'logueado',
		})
	} else {
		res.status(401).json({message: 'usuario no encontrado'})
    }
}

export{
	registerUser,
	authUser,
}
