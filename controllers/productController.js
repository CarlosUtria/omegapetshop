import Product from '../models/ProductModel.js'

// create product
const createProduct = async (req, res) => {
	const product = new Product({
		name: req.body.name,
		image: req.body.image,
		description: req.body.description,
	  	price: req.body.price,
		countInStock: req.body.countInStock,
	})

	product.save().then((createdProduct) => {
		res.status(201).json({ message: 'created Product'})
	}).catch((error) =>{
		res.status(500).json({ message: 'error '})
	})
	
}
// get all products
const getProducts = async (req, res) => {
	
	const productDB = await Product.find()
	.then(products => {
	  res.status(200).json(products)
	}).catch((err) =>{
	  res.status(500).json(err)
	})
}
 //get product by id
 const getProductById = async (req, res) => {
	const productId = await Product.findById(req.params.id)
	.then(product =>{
		res.status(200).json(product)
	}).catch(err =>{
		res.status(500).json({ message: 'Product not found' })
	})
}
// delete product
const deleteProductById = async (req, res) => {
	let id = req.params.id;

    Product.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).json({ message: 'Product not found'})
        }else{
            res.status(200).json({ message: 'Product removed' })
        }
    } );
}

// update product
const updateProduct = async (req, res) => {
	let id = req.params.id;
    let data = req.body;

    Product.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( { message: 'Product not found' });
        }else{
            res.status(200).send({ message: 'Product update' } );
        }
    } );
}

export{
	createProduct,
	getProducts,
	getProductById,
	deleteProductById,
	updateProduct,
}