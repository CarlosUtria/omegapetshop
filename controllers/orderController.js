import Order from '../models/orderModel.js'

// get all orders
const getOrders = async (req, res) => {
	const ordersList = await Order.find();
	if (!ordersList){
		res.status(500).json({message: false});
	}
	res.send(ordersList);
}

// create order
const createOrder = async (req, res) => {
	const {user, orderItems, shippingAddress, city, phone, totalPrice} = req.body


	if (orderItems.length === 0) {
		res.status(400).send('No order items')
	} else {
		const order = await Order.create({
			user: user, 
			orderItems: orderItems,
			shippingAddress, 
			city, 
			phone, 
			totalPrice

		})
		res.status(201).json({
			success: true,
			order,
		})
	}
}


// get Order INVOICED
const getSingleOrder = async (req, res) => {
	const order = await Order.findById(req.params.id).populate("orderItems.product", "-_id").populate("user", "name email -_id");
  
	if (!order) {
	  return res.json({message: "order not found"})
	}

	let totalAmount = 0;
	order.orderItems.forEach((order) => {
		totalAmount += order.quantity*order.product.price
	});

	order.orderStatus = "INVOICED"
	await order.save()
  
	res.status(200).json({
	  success: true,
	  totalAmount,
	  order,
	});
  };


export {
	getOrders,
	createOrder,
	getSingleOrder,
}