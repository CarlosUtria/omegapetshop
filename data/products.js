const products = [
	{
		"name": "Airpods Wireless Bluetooth Headphones",
		"image": "pokemon-azul.jpg",
		"description":
			"Lorem ipsum dolor sit amet, consectetur adipiscingolore magna aliqua.",
		"price": "890.99",
		"countInStock": "10"
	},
	{
		"name": "iPhone 11 Pro 256GB Memory",
		"image": "pokemon-oro.jpg",
		"description":
			"sapien eget mi proin sed libero enim sed faucibusque egestas diam in arcu cursus",
		"price": "5990.99",
		"countInStock": "7"
	},
	{
		"name": 'Cannon EOS 80D DSLR Camera',
		"image": 'pokemon-plata.jpg',
		"description":
			'praesent elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis',
		"price": 9290.99,
		"countInStock": 5
	},
	{
		"name": "Sony Playstation 4 Pro White Version",
		"image": "pokemon-rojo.jpg",
		"description":
			"auctor urna nunc id cursus metus aeam ultrices sagittis orci a",
		"price": 3990.99,
		"countInStock": 11
	},
	{
		"name": "Logitech G-Series Gaming Mouse",
		"image": "pokemon-verde.jpg",
		"description":
			"diam sit amet nisl s auctor elit sed vulris commodo",
		"price": 490.99,
		"countInStock": 7

	},
	{
		"name": "Amazon Echo Dot 3rd Generation",
		"image": "sonic.jpg",
		"description":
			"id velit ut tortor pretium viverra suspitae purus faucibus ornare suspendisse sed nisi",
		"price": 290.99,
		"countInStock": 0
	},
]

export default products
