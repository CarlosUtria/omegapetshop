import bcrypt from 'bcryptjs'

const users = [
	// Admin user
	{
		"name": "Melvin Doe",
		"email": "melvin@eg.com",
		"password": 12345,
		"isAdmin": true
	},
	// Standard users
	{
		"name": "Belle Doe",
		"email": "belle@eg.com",
		"password": 12345
	},
	{
		name: 'Mike Doe',
		email: 'mike@eg.com',
		password: bcrypt.hashSync('12345', 10), //  10 = num rounds
	},
]

export default users
