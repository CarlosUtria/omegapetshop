import express from 'express'
import {
	getProducts,
	getProductById,
	deleteProductById,
	updateProduct,
	createProduct,
} from '../controllers/productController.js'

const router = express.Router()


router.route('/').get(getProducts)
router.route('/:id').get(getProductById)
router.route('/create').post(createProduct)
router.route('/update/:id').put(updateProduct)
router.route('/delete/:id').get(deleteProductById)





export default router
