import express from 'express'
import {
	getOrders,
	createOrder,
	getSingleOrder,
} from '../controllers/orderController.js'

const router = express.Router()


router.route('/').get(getOrders)
router.route('/:id').get(getSingleOrder)
router.route('/create').post(createOrder)




export default router