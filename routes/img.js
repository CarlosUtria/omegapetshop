import express from 'express'
import path from 'path';

const router = express.Router();

// IMAGE PATH
const imgFolderPath = path.join(__dirname, '../img/');

//IMAGE
router.get('/:imgName', (req, res) => {
  const image = req.params.imgName;
  res.sendFile(`${imgFolderPath}${image}`);
});

module.exports = router;
